unit FormFiredac;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.MSAcc,
  FireDAC.Phys.MSAccDef, Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls, Data.DB,
  FireDAC.Comp.DataSet, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids,
  FireDAC.Comp.Client, Vcl.Buttons, FireDAC.Phys.ODBC, FireDAC.Phys.ODBCDef,
  FireDAC.VCLUI.Wait, FireDAC.Comp.UI;

type
  TFormExemplofiredac = class(TForm)
    FDQuery1: TFDQuery;
    FDConnection1: TFDConnection;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    FDQuery1cod_vend: TFDAutoIncField;
    FDQuery1nome: TWideStringField;
    FDQuery1endereco: TWideStringField;
    FDQuery1numero: TWideStringField;
    FDQuery1complemento: TWideStringField;
    FDQuery1cep: TWideStringField;
    FDQuery1celular: TWideStringField;
    FDQuery1telefone: TWideStringField;
    FDQuery1whatsup: TWideStringField;
    FDQuery1email: TWideStringField;
    FDQuery1data_inc: TSQLTimeStampField;
    FDQuery1data_alt: TSQLTimeStampField;
    FDQuery1data_exc: TSQLTimeStampField;
    FDQuery1data_cad: TSQLTimeStampField;
    FDQuery1marca: TWideStringField;
    FDQuery1usuario: TWideStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    btnFechar: TBitBtn;
    btnExcluir: TBitBtn;
    btnGravar: TBitBtn;
    btnCancelar: TBitBtn;
    btnAlterar: TBitBtn;
    btnIncluir: TBitBtn;
    spdUltimo: TBitBtn;
    spdProximo: TBitBtn;
    spdAnterior: TBitBtn;
    spdPrimeiro: TBitBtn;
    DataSource2: TDataSource;
    FDQuery2: TFDQuery;
    FDQuery2cod_vend: TFDAutoIncField;
    FDQuery2data_inc: TSQLTimeStampField;
    FDQuery2data_alt: TSQLTimeStampField;
    FDQuery2data_exc: TSQLTimeStampField;
    FDQuery2data_cad: TSQLTimeStampField;
    FDQuery2marca: TWideStringField;
    FDQuery2usuario: TWideStringField;
    FDQuery2nome: TWideStringField;
    FDQuery2endereco: TWideStringField;
    FDQuery2numero: TWideStringField;
    FDQuery2complemento: TWideStringField;
    FDQuery2cep: TWideStringField;
    FDQuery2celular: TWideStringField;
    FDQuery2telefone: TWideStringField;
    FDQuery2whatsup: TWideStringField;
    FDQuery2email: TWideStringField;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure btnIncluirClick(Sender: TObject);
    procedure spdAnteriorClick(Sender: TObject);
    procedure spdPrimeiroClick(Sender: TObject);
    procedure spdProximoClick(Sender: TObject);
    procedure spdUltimoClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure DBGrid1DrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormExemplofiredac: TFormExemplofiredac;

implementation

{$R *.dfm}

procedure TFormExemplofiredac.btnAlterarClick(Sender: TObject);
begin
FDQuery1.Edit;
end;

procedure TFormExemplofiredac.btnCancelarClick(Sender: TObject);
begin
FDQuery1.Cancel;
end;

procedure TFormExemplofiredac.btnFecharClick(Sender: TObject);
begin
close;
end;

procedure TFormExemplofiredac.btnGravarClick(Sender: TObject);
begin
FDQuery1.Post;
end;

procedure TFormExemplofiredac.btnIncluirClick(Sender: TObject);
begin
FDQuery1.Append;
end;

procedure TFormExemplofiredac.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
with FDQuery2 do
begin
//Connection := CONEXAO;
Close;
SQL.Clear;
SQL.Add('SELECT * FROM Vendedor WHERE cod_vend = :pCod');
ParamByName('pCod').Value := StrToIntDef(DBEdit1.Text,0);
Open;
end;
end;

procedure TFormExemplofiredac.DBGrid1DrawDataCell(Sender: TObject;
  const Rect: TRect; Field: TField; State: TGridDrawState);
begin
ShowScrollBar(DBGrid1.Handle,SB_HORZ,False);
end;

procedure TFormExemplofiredac.spdAnteriorClick(Sender: TObject);
begin
FDQuery1.Prior;
end;

procedure TFormExemplofiredac.spdPrimeiroClick(Sender: TObject);
begin
FDQuery1.First;
end;

procedure TFormExemplofiredac.spdProximoClick(Sender: TObject);
begin
FDQuery1.Next;
end;

procedure TFormExemplofiredac.spdUltimoClick(Sender: TObject);
begin
FDQuery1.Last;
end;

end.
